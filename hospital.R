## 11 Hospital.30.Day.Death..Mortality..Rates.from.Heart.Attack heart attack
## 17 Hospital.30.Day.Death..Mortality..Rates.from.Heart.Failure  heart fail
## 23 Hospital.30.Day.Death..Mortality..Rates.from.Pneumonia  pneumonia
csv<-function (){
  df <- read.csv("dataset/outcome-of-care-measures.csv")
  prefix <-"Hospital.30.Day.Death..Mortality..Rates.from."
  c <- names(df)
  idx <- which(startsWith(c,prefix))
  for(i in idx){
    df[,i] <- as.numeric(df[,i])
  }
  list(df=df,prefix=prefix)
}

getColNum<- function(outcome){
    if(outcome=="heart attack") 11
    else if(outcome=="heart fail") 17
    else if(outcome=="pneumonia") 12
    else 0
}


f <- function(csv){

  df <- csv$df
  best<-function(state,outcome){
    i <- (function(){
      myi <- getColNum(outcome)
      if(myi==0) stop("invalid outcome")
      myi
    })()
    
    state <- (function(){
      mystate <- df[which(df$State==state),]
      if(nrow(mystate)<1) stop("invalid state")
      mystate
    })()
    
    m<- (function(x){
      idx <- which(!is.na(state[,x]))
      min(state[idx,x])
    })(i)
    
    idx<- which(state[,i]==m)
    state[idx,2]
  }
  
  ## return a character vector containing the name of the hospital with the 5th lowest 30-day death rate
  ## for heart failure.
  rankhospital <- function(state, outcome, num = "best") {
    i<- getColNum(outcome)
    
    hospitalName<- (function(x,y){
      v<- df[which(df[7]==x & !is.na(df[y])),]
      v<- v[order(v[y],v[2]),]
      v[,2]
    })(state,i)
   
    (function(x,y){
      len <- length(y)
      if(num=="best") y[1]
      else if(num=="worst") y[len]
      else head(y,num)
    })(num,hospitalName)
  }
  
  rankall <- function(outcome, num = "best") {
    i<- getColNum(outcome)
    v<- df[which(!is.na(df[i])),c(2,7,i),]
    v<- v[order(v[3],v[2],v[1]),]
    v[,c(1,2)]
  }
  list(best=best,rankhospital=rankhospital,rankall=rankall)
}

# head(a[order(-a$A),],3)
# head(df[order(df$Hospital.30.Day.Death..Mortality..Rates.from.Heart.Failure),c(2,17)],5)
# nrow(df[which(df[7]=="CA") ,]) 341
# nrow(df[which(df[7]=="CA" & !is.na(df[11])) ,]) 227
# nrow(df[which(df[7]=="CA" & is.na(df[11])) ,]) 114
